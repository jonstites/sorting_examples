#!/usr/bin/env python3

import argh
import random

def sorter(insertion=False, merge=False, size=1000, num=100, debug=False):
    values = [random.randint(0, num) for i in range(size)]
    if debug:
        print(values[:10])
    if insertion:
        insertion_sort(values)
    elif merge:
        values = merge_sort(values)
    if debug:
        print(values == sorted(values))
        print(values[:10])

def merge_sort(values):
    if len(values) <= 1:
        return values
    midpoint = len(values) // 2
    left  = merge_sort(values[:midpoint])
    right = merge_sort(values[midpoint:])
    return merge(left, right)

def merge(left, right):
    i = j = 0
    sorted_values = []
    while (i < len(left)) or (j < len(right)):
        if i == len(left):
            sorted_values.append(right[j])
            j += 1
        elif j == len(right):
            sorted_values.append(left[i])
            i += 1
        elif left[i] < right[j]:
            sorted_values.append(left[i])
            i += 1
        else:
            sorted_values.append(right[j])
            j += 1
    return sorted_values
                      
def insertion_sort(values):
    for i in range(len(values)):
        lowest = i
        for j in range(i, len(values)):
            if values[j] < values[lowest]:
                lowest = j
        values[i], values[lowest] = values[lowest], values[i]
    return values

if __name__ == "__main__":
    argh.dispatch_command(sorter)
